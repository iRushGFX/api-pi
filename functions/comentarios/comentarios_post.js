const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.comentario) return util.bind(new Error('Enter your comentario!'))
    if (!body.data) return util.bind(new Error('Enter your data!'))
    if (!body.usuario_id) return util.bind(new Error('Enter your usuario_id!'))
    if (!body.post_id) return util.bind(new Error('Enter your post_id!'))

    const checkComentarioExist = await mysql.query('select * from comentario where comentario=?', [body.comentario])
    if (checkComentarioExist.length) return util.bind(new Error('An account with this comentario already exists!'))

    const insert = await mysql.query('insert into comentario (comentario, data, usuario_id, post_id) values (?,?,?,?)', [body.comentario, body.data, body.usuario_id, body.post_id])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
