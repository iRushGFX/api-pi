const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const posts = await mysql.query('select idPosts, titulo, imagem, data, usuario_id from posts where usuario_id=?', [event.pathParameters.usuario_id])
      return util.bind(posts.length ? posts[0] : {})
    }

    const posts = await mysql.query('select idPosts, titulo, imagem, data, usuario_id from posts')
    return util.bind(posts)
  } catch (error) {
    return util.bind(error)
  }
}
