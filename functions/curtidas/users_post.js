const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.data) return util.bind(new Error('Enter your data!'))
    if (!body.usuario_id) return util.bind(new Error('Enter your usuario_id!'))
    if (!body.post_id) return util.bind(new Error('Enter your post_id!'))
    

    const insert = await mysql.query('insert into curtidas (data, usuario_id, post_id) values (?,?,?)', [body.data, body.usuario_id, post_id])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
