const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.log) return util.bind(new Error('Enter your log!'))
    if (!body.data) return util.bind(new Error('Enter your date!'))
    if (!body.usuario_id) return util.bind(new Error('Enter your user_id!'))

    const insert = await mysql.query('insert into logs (log, data,usuario_id) values (?,?,?)', [body.logs, body.data,body.usuario_id])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
